import { Component, OnInit, ViewChild } from '@angular/core';
import { DashboardService } from '../dashboard.service';
import {  MatPaginator } from '@angular/material/paginator';
import {  MatTableDataSource } from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import { IssuesService } from 'src/app/modules/issues.service';
import { Router } from '@angular/router';
import { HarvestService } from 'src/app/_services/harvest.service';
import { faUserSecret,faUsers,faHatCowboy,faBook,faSeedling,faLeaf} from '@fortawesome/free-solid-svg-icons';
import { Subscriber } from 'src/app/model/subscriber.model';
// import { MatTableDataSource } from '@angular/material/table';
// import { Issue } from '../../model/issue.model';
import { User } from '../../model/user.model';

//import { User } from './../../model/user.model';
import { Harvest } from 'src/app/model/harvest.model';
import { TokenStorageService } from 'src/app/_services/token-storage.service';
import { SubscriberService } from 'src/app/_services/subscriber.service';
import { PlantService } from 'src/app/_services/plant.service';

@Component({
  selector: 'app-Subscribers',
  templateUrl: './Subscribers.component.html',
  styleUrls: ['./Subscribers.component.scss']
})
export class SubscribersComponent implements OnInit {

  subscribers: Subscriber[];
   displayedColumns=['name','city','phone','actions'];

  constructor(private subscriberService :SubscriberService, private router:Router) { }

  ngOnInit(): void {
    this.subscriberService.getAll().subscribe((subscribers)=>{
     console.log(subscribers); 
    })
   
     this.fetchSubscribers();

  }

  fetchSubscribers(){
    this.subscriberService
        .getAll()
        .subscribe((data:Subscriber[])=>{
          this.subscribers = data;
          console.log("Data requested");
          console.log(this.subscribers);
        });    
  }

  editSubscriber(id){
    
      this.router.navigate([`/subedit/${id}`]);      
  }

  deleteSubscriber(id){
     if (confirm("Are you sure you want to delete?"))
    this.subscriberService.delete(id).subscribe(()=>{
        this.fetchSubscribers();
    })
  }

  addSubLoc(id){
    this.router.navigate([`/farmlocationcreate/${id}`]);      
  }


}
