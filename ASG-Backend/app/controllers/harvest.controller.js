const db = require("../models");
const Harvest = db.harvests;

// Create and Save a new Harvest
exports.create = (req, res) => {
  // Validate request
  if (!req.body.operation) {
    res.status(400).send({ message: "Content can not be empty!" });
    return;
  }

  // Create a Harvest
  const harvest = new Harvest({
    
        farmer:req.body.farmer,
        crop:req.body.crop,
        operation:req.body.operation,
        quantity:req.body.quantity,
        quality:req.body.quality ,
        owner: req.userId, farmlocation: req.body.farmlocationId
        //AdditionalInfo:req.body.AdditionalInfo
    
     });

  // Save Harvest in the database
  harvest
    .save(harvest)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Harvest."
      });
    });
};

// Retrieve all Harvests from the database.
exports.findAll = (req, res) => {
  const operation = req.query.operation;
  var condition = operation ? { operation: { $regex: new RegExp(operation), $options: "i" } } : {};

  Harvest.find({ owner: req.userId })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving harvests."
      });
    });
};

// Find a single Harvest with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

  Harvest.findById(id)
    .then(data => {
      if (!data)
        res.status(404).send({ message: "Not found Harvest with id " + id });
      else res.send(data);
    })
    .catch(err => {
      res
        .status(500)
        .send({ message: "Error retrieving Harvest with id=" + id });
    });
};

// Update a Harvest by the id in the request
exports.update = (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: "Data to update can not be empty!"
    });
  }

  const id = req.params.id;

  Harvest.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Cannot update Harvest with id=${id}. Maybe Harvest was not found!`
        });
      } else res.send({ message: "Harvest was updated successfully." });
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating Harvest with id=" + id
      });
    });
};

// Delete a Harvest with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  Harvest.findByIdAndRemove(id, { useFindAndModify: false })
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Cannot delete Harvest with id=${id}. Maybe Harvest was not found!`
        });
      } else {
        res.send({
          message: "Harvest was deleted successfully!"
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete Harvest with id=" + id
      });
    });
};

// Delete all Harvests from the database.
exports.deleteAll = (req, res) => {
  Harvest.deleteMany({})
    .then(data => {
      res.send({
        message: `${data.deletedCount} Harvests were deleted successfully!`
      });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all harvests."
      });
    });
};

exports.harvestCount = (rep,res,next) =>{
    
  Harvest.aggregate([
    {
      $group: {
        _id:1,
        count:{
          $sum:1
        }
      }
    }
   
  ],
  (error,data)=>{
    if (error){
      return next(error);
    } else {
      res.json(data);
    }

  }
  )

}


exports.harvestTotal = (rep,res,next) =>{
    
  Harvest.aggregate([
    {
      $group: { _id:{ farmer:"$farmer",crop:"$crop"},
        
      
      total:{
          $sum:"$quantity"
        }

      }
    }
   
  ],
  (error,data)=>{
    if (error){
      return next(error);
    } else {
      res.json(data);
    }

  }
  )

}



exports.allHarvestTotal = (rep,res,next) =>{
    
  Harvest.aggregate([
    {
      $group: { _id:1,
        
      
      total:{
          $sum:"$quantity"
        }

      }
    }
   
  ],
  (error,data)=>{
    if (error){
      return next(error);
    } else {
      res.json(data);
    }

  }
  )

}
// Find all published Harvests
exports.findAllPublished = (req, res) => {
  Harvest.find({ published: true })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving harvests."
      });
    });
};


//specific subscriber count 

exports.harvestSpecificCount =  (req,res,next) =>{
  Harvest.find({ owner: req.userId })
  .exec((err, harvest) => {
   let count = harvest.length;
   res.status(200).send((count).toString());
 })
};
 

exports.allHarvestTotalEach = (req,res,next) =>{
    
  Harvest.aggregate([

   
    { $group: { _id:"$owner",total:{ $sum:"$quantity"
        }

      }
    },
    {"$sort": {"_id": -1}},
     {"$limit": 5}
   
  ],
  (error,data)=>{
      
      if (error){
       
        res.json(error);
      }
      else 
      res.json(data);
    }
  
  )

}
  