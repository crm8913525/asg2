const db = require("../models");
const Farmlocation = db.farmlocations;
//const Subscriber= db.subscribers;

// Create and Save a new Farmlocation
exports.create = (req, res) => {
  // Validate request
  if (!req.body.longitude) {
    res.status(400).send({ message: "Content can not be empty!" });
    return;
  }

  // Create a Farmlocation
  const farmlocation = new Farmlocation({
    longitude: req.body.longitude,
    latitude: req.body.latitude,
    //subscriber:req.body.subscriberId  
     });

  // Save Farmlocation in the database
  farmlocation
    .save(farmlocation)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Farmlocation."
      });
    });
};

// Retrieve all Farmlocations from the database.
exports.findAll = (req, res) => {
  const longitude = req.query.longitude;
  var condition = longitude ? { longitude: { $regex: new RegExp(longitude), $options: "i" } } : {};

  Farmlocation.find(condition)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving farmlocations."
      });
    });
};

// Find a single Farmlocation with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

  Farmlocation.findById(id)
    .then(data => {
      if (!data)
        res.status(404).send({ message: "Not found Farmlocation with id " + id });
      else res.send(data);
    })
    .catch(err => {
      res
        .status(500)
        .send({ message: "Error retrieving Farmlocation with id=" + id });
    });
};

// Update a Farmlocation by the id in the request
exports.update = (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: "Data to update can not be empty!"
    });
  }

  const id = req.params.id;

  Farmlocation.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Cannot update Farmlocation with id=${id}. Maybe Farmlocation was not found!`
        });
      } else res.send({ message: "Farmlocation was updated successfully." });
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating Farmlocation with id=" + id
      });
    });
};

// Delete a Farmlocation with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  Farmlocation.findByIdAndRemove(id, { useFindAndModify: false })
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Cannot delete Farmlocation with id=${id}. Maybe Farmlocation was not found!`
        });
      } else {
        res.send({
          message: "Farmlocation was deleted successfully!"
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete Farmlocation with id=" + id
      });
    });
};

// Delete all Farmlocations from the database.
exports.deleteAll = (req, res) => {
  Farmlocation.deleteMany({})
    .then(data => {
      res.send({
        message: `${data.deletedCount} Farmlocations were deleted successfully!`
      });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all farmlocations."
      });
    });
};

// Find all published Farmlocations
exports.findAllPublished = (req, res) => {
  Farmlocation.find({ published: true })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving farmlocations."
      });
    });
};
